﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;


namespace lab_3
{
    public class Program
    {

        static void Main(string[] args)
        {
            IDeque<int> deq = new Deque<int>();
            //deq.PushBegin(1);
            //deq.PushBegin(2);
            //deq.PushBegin(3);
            //deq.PushBack("предпоследний");
            //deq.PushBack("последний");
            //Console.WriteLine(deq.first());
            //Console.WriteLine(deq.Print());
            //deq.DelBegin();
            //Console.WriteLine(deq.first());
            //deq.DelBegin();
            //Console.WriteLine(deq.first());
            //deq.DelBack();
            //Console.WriteLine(deq.Print());
            if (deq is IDeque<int>)
            {
                deq.PushBegin(new int[] { 5, 7, 9, 2, 7, 0, -7, -2, 5, 8, 5, 2, 1, -6 });
                Console.WriteLine(MinPipeline(deq));
                Console.WriteLine(MinForeach(deq));
            }
            Console.ReadKey();
        }
        public static int MinPipeline(IDeque<int> deq)
        {

            try
            {
                return deq.Where(x => x <= 0).Min();
            }
            catch
            {
                return 1;
            }
        }
        public static int MinForeach(IDeque<int> deq)
        {
            int min = 1;
            foreach (int item in deq)
            {
                if (item <= 0 && item < min)
                {
                    min = item;
                }
            }
            return min;
        }

    }

    public interface IDeque<T> : IEnumerable<T>, ICloneable
    {
        public int size { get; }
        public void PushBegin(T value);
        public void PushBack(T value);
        public void PushBegin(T[] mas);
        public void PushBack(T[] mas);
        public void DelBegin();
        public void DelBack();

        public int Size();
        public T first();
        public T last();
        public string Print();

    }
    [Serializable]
    public class Deque<T> : IDeque<T>

    {
        T[] mas;
        private int Size = 0;
        public int size
        {
            get
            {
                return Size;
            }
        }
        public int Lenght
        {
            get => (int)list.Count;
        }

        private List<T> list { get; set; }
        public Deque()
        {
            list = new List<T>();
        }
        int IDeque<T>.Size() => list.Count;

        void IDeque<T>.PushBegin(T value)
        {
            list.Insert(0, value);
        }
        void IDeque<T>.PushBack(T value)
        {
            list.Add(value);
        }


        void IDeque<T>.DelBegin()
        {
            list.RemoveAt(0);
        }
        void IDeque<T>.DelBack()
        {
            list.RemoveAt(Convert.ToInt32(list.Count - 1));
        }
        T IDeque<T>.first() => list[0];
        T IDeque<T>.last() => list[list.Count - 1];
        string IDeque<T>.Print()
        {
            string arr = "";
            foreach (var a in list)
            {
                arr += a;
            }
            return arr;
        }
        public T this[int pos]
        {
            get
            {
                return list[pos];
            }
            set
            {

            }
        }
        public Deque(T[] a)
        {
            list = new List<T>();
            for (int i = 0; i < a.Length; i++)
            {
                list.Add(a[i]);
            }
        }
        public static Deque<T> Sort<T>(ref Deque<T> a) // 1
                  where T : IComparable<T> // 2
        {
            for (int i = 0; i < a.Lenght - 1; i++)
            {
                int buf = i;
                for (int j = i++; j < a.Lenght; j++)
                {
                    if (a[j].CompareTo(a[buf]) > 0)
                    {
                        buf = j;
                    }
                }
                T Lbuf = a[i];
                a[i] = a[buf];
                a[buf] = Lbuf;
            }
            return a;
        }
        public IEnumerator<T> GetEnumerator()
        {
            //for (int i = 0; i < list.Count; i++)
            //{
            //    yield return list[(i)];
            //}
            return ((IEnumerable<T>)list).GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return list.GetEnumerator();
        }
        public object Clone()
        {
            Deque<T> deq = new Deque<T>();
            List<T> res = new List<T>();
            for (int i = 0; i < this.list.Count; i++)
            {
                res.Add(this.list[i]);
            }
            deq.list = res;
            return deq;
        }
        public object CallClone()
        {
            return Clone();
        }
        public Deque<T> LocalClone()
        {
            return (Deque<T>)this.MemberwiseClone();
        }
        public void PushBack(T[] mas)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                list.Add(mas[i]);
            }
        }
        public void PushBegin(T[] mas)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                list.Insert(0, mas[i]);
            }
        }

    }
}

