using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using lab_3;
using NUnit.Framework;

namespace test
{
    [TestFixture]
    public class Test
    {
        [Test()]
        public void testPushBegin()
        {
            Deque<object> deq = new Deque<object>();
            ((IDeque<object>)deq).PushBegin(1);
            ((IDeque<object>)deq).PushBegin(2);
            ((IDeque<object>)deq).PushBack(3);
            string act = Convert.ToString(((IDeque<object>)deq).first());
            string exp = "2";
            Assert.AreEqual(exp, act);
        }
        [Test()]
        public void testPrint()
        {
            Deque<object> deq = new Deque<object>();
            ((IDeque<object>)deq).PushBegin(1);
            ((IDeque<object>)deq).PushBegin(2);
            ((IDeque<object>)deq).PushBegin(3);
            string act = ((IDeque<object>)deq).Print();
            string exp = "321";
            Assert.AreEqual(exp, act);
        }
        [Test()]
        public void testDelBegin()
        {
            Deque<object> deq = new Deque<object>();
            ((IDeque<object>)deq).PushBegin(1);
            ((IDeque<object>)deq).PushBegin(2);
            ((IDeque<object>)deq).PushBegin(3);
            string exp = "21";
            ((IDeque<object>)deq).DelBegin();
            string act = ((IDeque<object>)deq).Print();
            Assert.AreEqual(exp, act);
        }
        [Test()]
        public void testPushBack()
        {
            Deque<object> deq = new Deque<object>();
            ((IDeque<object>)deq).PushBegin(1);
            ((IDeque<object>)deq).PushBegin(2);
            ((IDeque<object>)deq).PushBack(3);
            string act = Convert.ToString(((IDeque<object>)deq).last());
            string exp = "3";
            Assert.AreEqual(exp, act);
        }
        [Test()]
        public void testDelBack()
        {
            Deque<object> deq = new Deque<object>();
            ((IDeque<object>)deq).PushBegin(1);
            ((IDeque<object>)deq).PushBegin(2);
            ((IDeque<object>)deq).PushBegin(3);
            string exp = "32";
            ((IDeque<object>)deq).DelBack();
            string act = ((IDeque<object>)deq).Print();
            Assert.AreEqual(exp, act);
        }
        [Test()]
        public void BoolTest()
        {
            Deque<bool> deq = new Deque<bool>(new[] { true, false, true });
            bool exp = true;
            Deque<bool>.Sort(ref deq);
            bool act = Convert.ToBoolean(((IDeque<bool>)deq).first());
            Assert.AreEqual(exp, act);
        }
        [Test()]
        public void StringTest()
        {
            Deque<string> deq = new Deque<string>(new[] { "qwe", "asd", "zxc" });
            string exp = "qwe";
            Deque<string>.Sort(ref deq);
            string act = Convert.ToString(((IDeque<string>)deq).first());
            Assert.AreEqual(exp, act);
        }
        [Test()]
        public void DoubleTest()
        {
            Deque<double> deq = new Deque<double>(new[] { 1.5, 2.5, 63 });
            double exp = 1.5;
            Deque<object>.Sort(ref deq);
            double act = Convert.ToDouble(((IDeque<double>)deq).first());
            Assert.AreEqual(exp, act);
        }
        [Test()]
        public void IntTest()
        {
            Deque<int> deq = new Deque<int>(new[] { 1, 5, 6 });
            int exp = 1;
            Deque<int>.Sort(ref deq);
            int act = Convert.ToInt32(((IDeque<int>)deq).first());
            Assert.AreEqual(exp, act);
        }
        [Test()]
        public void NumEnumeratorTest()
        {
            Deque<int> deq = new Deque<int>(new[] { 10, 50, 6, 20, 10, 2 });
            List<int> exp = new List<int>() { 10, 50, 6, 20, 10, 2 };
            int i = 0;
            foreach (var item in deq)
            {
                Assert.AreEqual(exp[i], item);
                i++;
            }
        }
        [Test()]
        public void StringCloneTest()
        {
            IDeque<string> deq = new Deque<string>(new[] { "qwe", "asd", "zxc" });
            IDeque<string> exp = (Deque<string>)deq.Clone();
            deq.DelBegin();
            deq.DelBegin();
            string act = Convert.ToString(deq.last());
            string res = Convert.ToString(exp.last());
            Assert.AreEqual(res, act);
        }
        [Test()]
        public void MaxTestEmptyArray()
        {
            Deque<object> deq = new Deque<object>();
            Assert.Throws<ArgumentOutOfRangeException>(() => Convert.ToInt32(((IDeque<object>)deq).first()));
        }
        [Test()]
        public void FirstSer()
        {
            IDeque<int> deq = new Deque<int>(new int[] { 9, 5, 2, 6, 7});
            BinaryFormatter call = new BinaryFormatter();
            FileStream fs = new FileStream("lab7.bin", FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            call.Serialize(fs, deq);
            fs.Close();
            FileStream fs1 = new FileStream("lab7.bin", FileMode.Open);
            IDeque<int> deq1 = (Deque<int>)call.Deserialize(fs1);
            fs1.Close();
            string exp = "9";
            Assert.AreEqual(9, deq1.first());
        }
        [Test()]
        public void LastSer()
        {
            IDeque<int> deq = new Deque<int>(new int[] { 2, 6, 7, 9, 5 });
            BinaryFormatter call = new BinaryFormatter();
            FileStream fs = new FileStream("lab7.bin", FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            call.Serialize(fs, deq);
            fs.Close();
            FileStream fs1 = new FileStream("lab7.bin", FileMode.Open);
            IDeque<int> deq1 = (Deque<int>)call.Deserialize(fs1);
            fs1.Close();
            string exp = "5";
            Assert.AreEqual(5, deq1.last());
        }
        [Test()]
        public void ErrFirstSer()
        {
            IDeque<int> deq = new Deque<int>();
            BinaryFormatter call = new BinaryFormatter();
            FileStream fs = new FileStream("lab7.bin", FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            call.Serialize(fs, deq);
            fs.Close();
            FileStream fs1 = new FileStream("lab7.bin", FileMode.Open);
            IDeque<int> deq1 = new Deque<int>();
            fs1.Close();
            Assert.Throws<ArgumentOutOfRangeException>(() => { deq1.first(); });
        }
        [Test()]
        public void ErrLastSer()
        {
            IDeque<int> deq = new Deque<int>();
            BinaryFormatter call = new BinaryFormatter();
            FileStream fs = new FileStream("lab7.bin", FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            call.Serialize(fs, deq);
            fs.Close();
            FileStream fs1 = new FileStream("lab7.bin", FileMode.Open);
            IDeque<int> deq1 = new Deque<int>();
            fs1.Close();
            Assert.Throws<ArgumentOutOfRangeException>(() => { deq1.last(); });
        }
    }
    public class QueueTests
    {
        [Test]
        public void EnqueueInNotEmptyQueue()
        {
            Queue<int> que = new Queue<int>();
            que.Enqueue(5);
            que.Enqueue(8);
            que.Enqueue(1);
            que.Enqueue(2);
            int exp = 5;
            int res = que.Dequeue();
            Assert.AreEqual(exp, res);
        }
        [Test]
        public void DequeueInNotEmptyQueue()
        {
            Queue<int> que = new Queue<int>();
            que.Enqueue(5);
            que.Enqueue(8);
            que.Enqueue(1);
            que.Enqueue(2);
            int exp = 5;
            int res = que.Dequeue();
            Assert.AreEqual(exp, res);
        }
        [Test]
        public void PeekInNotEmptyQueue()
        {
            Queue<int> que = new Queue<int>();
            que.Enqueue(5);
            que.Enqueue(8);
            que.Enqueue(1);
            que.Enqueue(2);
            int exp = 5;
            int res = que.Peek();
            Assert.AreEqual(exp, res);
        }
        [Test]
        public void DequeueInEmptyQueue()
        {
            Queue<int> que = new Queue<int>();
            Assert.Throws<NullReferenceException>(() => que.Dequeue());
        }
        [Test]
        public void PeekInEmptyQueue()
        {
            Queue<int> deq = new Queue<int>();
            Assert.Throws<NullReferenceException>(() => deq.Peek());
        }
        [Test]
        public void CountNotEmptyTest()
        {
            Queue<int> que = new Queue<int>();
            que.Enqueue(5);
            que.Enqueue(8);
            que.Enqueue(1);
            que.Enqueue(2);
            int exp = 4;
            int res = que.Count;
            Assert.AreEqual(exp, res);
        }
        [Test]
        public void CountEmptyTest()
        {
            Queue<int> que = new Queue<int>();
            int exp = 0;
            int res = que.Count;
            Assert.AreEqual(exp, res);
        }
        [Test]
        public void ClearQueue()
        {
            Queue<int> que = new Queue<int>();
            que.Enqueue(5);
            que.Enqueue(8);
            que.Enqueue(1);
            que.Enqueue(2);
            Queue<int> d = new Queue<int>();
            que.Clear();
            Assert.AreEqual(que, d);
        }
        [Test]
        public void ContainsInQueue()
        {
            Queue<int> que = new Queue<int>();
            que.Enqueue(5);
            que.Enqueue(8);
            que.Enqueue(1);
            que.Enqueue(2);
            bool exp = true;
            bool res = que.Contains(5);
            Assert.AreEqual(exp, res);
        }
        [Test]
        public void NotContainsInQueue()
        {
            Queue<int> que = new Queue<int>();
            que.Enqueue(5);
            que.Enqueue(8);
            que.Enqueue(1);
            que.Enqueue(2);
            bool exp = false;
            bool res = que.Contains(9);
            Assert.AreEqual(exp, res);
        }
        [Test]
        public void QueueToArray()
        {
            Queue<int> que = new Queue<int>();
            que.Enqueue(5);
            que.Enqueue(8);
            que.Enqueue(1);
            que.Enqueue(2);
            int[] res = que.ToArray();
            int[] exp = new int[] { 5, 8, 1, 2 };
            Assert.AreEqual(exp, res);
        }
        [Test]
        public void QueueCopyToNotNullArray()
        {
            Queue<int> que = new Queue<int>();
            que.Enqueue(5);
            que.Enqueue(8);
            que.Enqueue(1);
            que.Enqueue(2);
            int[] res = new int[que.Count];
            que.CopyTo(res, 0);
            int[] exp = new int[] { 5, 8, 1, 2 };
            Assert.AreEqual(exp, res);
        }
        [Test]
        public void QueueCopyToNullArray()
        {
            Queue<int> que = new Queue<int>();
            que.Enqueue(5);
            que.Enqueue(8);
            que.Enqueue(1);
            que.Enqueue(2);
            int[] res = null;
            Assert.Throws<ArgumentNullException>(() => que.CopyTo(res, 0));
        }
        [Test]
        public void QueueForeach()
        {
            Queue<int> que = new Queue<int>();
            que.Enqueue(5);
            que.Enqueue(8);
            que.Enqueue(1);
            que.Enqueue(2);
            int res = 0;
            foreach (var item in que)
            {
                res++;
            }
            int exp = que.Count;
            Assert.AreEqual(exp, res);
        }
    }
}
